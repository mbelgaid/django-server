#!/bin/sh

name=$1
networkname=aloha

for i in `ls implementations | egrep "$name.*dk"` ;
    do 
    echo "building chakibmed/icse_${i%*.dk}:django2"
    docker build --rm -f implementations/$i -t chakibmed/icse_${i%*.dk}:django2 .
    done

if  [ -n "$name" ] ; 
then 
    docker run --rm -d --name $name --net $networkname -P chakibmed/icse_$name:django2   
fi 