# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets
from rest_framework.response import Response
from people.models import People
from people.serializers import PeopleSerializer
from django_filters.rest_framework import DjangoFilterBackend


class PeopleViewSet(viewsets.ModelViewSet):
    serializer_class = PeopleSerializer
    queryset = People.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filter_fields = '__all__'

  